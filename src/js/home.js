import Vue from 'vue';
import csvtojson from './csvtojson.js';

new Vue({
  el: '#mentor-section',
  data: {
    speakers: [],
    key: '2PACX-1vRjQO3p40GxbUqbs5BxUkYtwh2aJubeh3Aam7MtbkZ5GrjSXftga_dSBH9wKswin6ObNgozn1ug7q9a',
  },
  computed: {
    current: function() {
      return this.sort(this.speakers, 2)
    },
    previous: function() {
      return this.sort(this.speakers, 1)
    }
  },
  methods: {
    ffs: function(number) {
      return this.sort(this.speakers, number)
    },
    sort: function(arr, event) {

      // Set slice() to avoid to generate an infinite loop!
      var sorted = arr.slice().sort(function(a, b) {
        return a.order - b.order;
      });

      sorted = sorted.filter(function(item) {
        return item.order > 0
      });

      return sorted.filter(function(item) {
        return item.event == event;
      });

    },
    loadData: function(item, key, gid) {
      var vm = this;
      fetch(`https://docs.google.com/spreadsheets/d/e/${key}/pub?gid=${gid}&single=true&output=csv`)
        .then(response => response.text())
        .then(csv => csvtojson(csv))
        .then(function(json) {
          vm[item] = JSON.parse(json);
        });
    },
  },
  mounted() {
    this.loadData('speakers', this.key, '0');
  }

})

document.querySelector("#title svg").setAttribute('viewBox', '0 0 1366 728');