module.exports = {
  "collapseAttributeWhitespace": false,
  "collapseBooleanAttributes": false,
  "collapseWhitespace": "conservative",
  "custom": [],
  "deduplicateAttributeValues": true,
  "mergeScripts": true,
  "mergeStyles": true,
  "removeUnusedCss": true,
  "minifyJson": true,
  "minifySvg": false,
  "removeEmptyAttributes": true,
  "removeRedundantAttributes": true,
  "removeComments": false
}